'use strict'
Hamburger.SIZE_SMALL = {
    price: 50,
    calorie: 20,
    name: 'SIZE_SMALL'
}
Hamburger.SIZE_LARGE = {
    price: 100,
    calorie: 40,
    name: 'SIZE_LARGE'
}
Hamburger.STUFFING_CHEESE = {
    price: 10,
    calorie: 20,
    name: 'STUFFING_CHEESE'
}
Hamburger.STUFFING_SALAD = {
    price: 20,
    calorie: 5,
    name: 'STUFFING_SALAD'
}
Hamburger.STUFFING_POTATO = {
    price: 15,
    calorie: 10,
    name: 'STUFFING_POTATO'
}
Hamburger.TOPPING_MAYO = {
    price: 20,
    calorie: 5,
    name: 'TOPPING_MAYO'
}
Hamburger.TOPPING_SPICE = {
    price: 15,
    calorie: 0,
    name: 'TOPPING_SPICE'
}
var currentBurger = {};



// base class
function Hamburger(size, stuffing) {

    try {
        if (((size == Hamburger.SIZE_LARGE) || (size == Hamburger.SIZE_SMALL))
            &&
            (stuffing == Hamburger.STUFFING_CHEESE) || (stuffing == Hamburger.STUFFING_POTATO) || (stuffing == Hamburger.STUFFING_SALAD)) {

            this.size = size;
            this.stuffing = stuffing;

            this.currentBurger = {
                size: this.size,
                stuffing: this.stuffing,
                topping: []
            }
        } else {
            var message = 'type error';
            throw new HamburgerException(message);
        };
    }
    catch (e) {
        console.log(e);
    }
    // constance




    // add topping method
    Hamburger.prototype.addTopping = function (topping) {

        try {
            if ((topping == Hamburger.TOPPING_MAYO) || (topping == Hamburger.TOPPING_SPICE)) {

                this.topping = topping;
                hamburger.currentBurger.topping.push(this.topping);


            } else {

                var message = 'type error';
                throw new HamburgerException(message);
            }




        }
        catch (e) {
            console.log(e);

        }
    }

    // remove topping method
    Hamburger.prototype.removeTopping = function (topping) {

        try {
            var flag = true;
            var index = 0;
            if ((topping == Hamburger.TOPPING_MAYO) || (topping == Hamburger.TOPPING_SPICE)) {
                this.topping = topping;

                for (var i = 0; i <= hamburger.currentBurger.topping.length; i++) {
                    if ((this.topping.name != hamburger.currentBurger.topping[i].name) || (hamburger.currentBurger.topping.length == 0)) {
                        flag = false;

                    } else {
                        flag = true;
                        index = i;
                        break;
                    }

                };
                if (flag == false) {
                    var message = 'type error';
                    throw new HamburgerException(message);

                } else {
                    delete hamburger.currentBurger.topping[index];
                }



            } else {
                var message = 'type error';
                throw new HamburgerException(message);
            }

        }

        catch (e) {
            console.log(e);
        }
    }

    // get toppings method
    Hamburger.prototype.getToppings = function () {
        var arrToppings = [];
        for (var i = 0; i <= hamburger.currentBurger.topping.length; i++) {
            if (hamburger.currentBurger.topping[i] === undefined) {
                continue;
            } else {
                arrToppings.push(hamburger.currentBurger.topping[i].name);


            }

        }
        return arrToppings;
    }
    // get size method
    Hamburger.prototype.getSize = function () {
        try {
            if (hamburger.currentBurger.size.name) {

            } else {
                var message = 'type error';
                throw new HamburgerException(message);
            }
        }
        catch (e) {
            console.log(e);

        }



    }
    // get stuffing method
    Hamburger.prototype.getStuffing = function () {
        try {
            if (hamburger.currentBurger.stuffing.name) {

            } else {
                var message = 'type error';
                throw new HamburgerException(message);
            }
        }
        catch (e) {
            console.log(e);

        }
    }

    Hamburger.prototype.calculatePrice = function () {
        try {
            if (hamburger.currentBurger) {
                var sumPrice = 0;
                for (var prop in hamburger.currentBurger) {

                    if (!Array.isArray(hamburger.currentBurger[prop])) {
                        sumPrice += hamburger.currentBurger[prop].price;

                    }

                    else if (Array.isArray(hamburger.currentBurger[prop])) {
                        for (var i = 0; i < hamburger.currentBurger[prop].length; i++) {
                            if (hamburger.currentBurger[prop][i] === undefined) {
                                continue;
                            }

                            sumPrice += hamburger.currentBurger[prop][i].price;

                        }
                    }
                }
                console.log(sumPrice);

            } else {
                var message = 'type error';
                throw new HamburgerException(message);
            }
        }
        catch (e) {

        }


    }

    Hamburger.prototype.calculateCalories = function () {
        try {
            if (hamburger.currentBurger) {
                var sumCalories = 0;
                for (var prop in hamburger.currentBurger) {

                    if (!Array.isArray(hamburger.currentBurger[prop])) {
                        sumCalories += hamburger.currentBurger[prop].calorie;

                    }

                    else if (Array.isArray(hamburger.currentBurger[prop])) {
                        for (var i = 0; i < hamburger.currentBurger[prop].length; i++) {
                            if (hamburger.currentBurger[prop][i] === undefined) {
                                continue;
                            }

                            sumCalories += hamburger.currentBurger[prop][i].calorie;

                        }
                    }
                }
                console.log(sumCalories);

            } else {
                var message = 'type error';
                throw new HamburgerException(message);
            }
        }
        catch (e) {

        }
    }

}

// exeption function
function HamburgerException(message) {
    this.name = 'Error in Hamburger =)';
    this.message = message;
    this.stack = (new Error().stack);
}
HamburgerException.prototype = new Error;

var hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE)
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.removeTopping(Hamburger.TOPPING_MAYO);



hamburger.getSize();






hamburger.calculatePrice();
hamburger.calculateCalories();


